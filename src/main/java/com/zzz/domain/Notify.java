package com.zzz.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class Notify implements Serializable {
    private int id;
    private String username;
    private String notifyType;
    private String toBeNotify;
    private String alreadyNotify;
    private String status;
    private String createTime;
    private String updateTime;
}
