package com.zzz.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Set;

@Component
public class AppConstant {
    public static final String jwtExpireTime = "talkSystemJwtExpireTime";
    public static final String sign = "sign"; //JWT的签名字符串
    public static final Integer successCode = 20000; //操作成功
    public static final Integer failCode = 20001; //操作失败
    public static final Integer jwtExceptionCode = 20002; //令牌失效
    public static final Integer jwtTokenNullCode = 20003; //令牌token为空
    public static final Integer duplicateKeyExceptionCode = 20004; //数据库插入重复的数据
    public static final Integer balanceErrorCode = 20005; //金额不足
    public static final Integer numsErrorCode = 20006; //商品数量不足
    public static final Integer overRequestException = 20007; //请求次数过多
    public static final String queueName = "notify";
}
