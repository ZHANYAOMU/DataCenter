package com.zzz.controller;

import com.zzz.common.Result;
import com.zzz.domain.Notify;
import com.zzz.param.NotifyParam;
import com.zzz.service.NotifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/data")
@Slf4j
public class MainController {
    @Autowired
    private NotifyService notifyService;
    @PostMapping("/notify")
    public Result<?> notify(@RequestBody Notify notify) {
        notifyService.addNotify(notify);
        return Result.success("success");
    }
    @PostMapping("/updateNotifyUser")
    public Result<?> updateNotifyUser(@RequestBody NotifyParam notifyParam) {
        notifyService.updateNotifyUser(notifyParam.getId(), notifyParam.getUsername() + "|");
        return Result.success("success");
    }
    @PostMapping("/test")
    public Result<?> test() {
        return Result.success("test");
    }
}
