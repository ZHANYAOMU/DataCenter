package com.zzz.common;

import com.zzz.constant.AppConstant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private Integer code;
    private String msg;
    private Long totalCount;
    private T data;
    public static <T> Result<T> success() {
        return new Result<>(AppConstant.successCode, "success", 0L, null);
    }
    public static <T> Result<T> success(T data) {
        return new Result<>(AppConstant.successCode, "success", 0L, data);
    }

    public static <T> Result<T> success(String msg) {
        return new Result<>(AppConstant.successCode, msg, 0L, null);
    }
    public static <T> Result<T> success(T data, String msg) {
        return new Result<>(AppConstant.successCode, msg, 0L, data);
    }
    public static <T> Result<T> success(T data, String msg, Long totalCount) {
        return new Result<>(AppConstant.successCode, msg, totalCount, data);
    }
    public static <T> Result<T> fail() {
        return new Result<>(AppConstant.failCode, "fail", 0L, null);
    }
    public static <T> Result<T> fail(String msg) {
        return new Result<>(AppConstant.failCode, msg, 0L, null);
    }
    public static <T> Result<T> fail(Integer code, String msg) {
        return new Result<>(code, msg, 0L, null);
    }
    public static <T> Result<T> fail(String msg, T data) {
        return new Result<>(AppConstant.failCode, msg, 0L, data);
    }
    public static <T> Result<T> fail(Integer code,String msg, T data) {
        return new Result<>(code, msg, 0L, data);
    }
    public static <T> Result<T> fail(T data) {
        return new Result<>(AppConstant.failCode, "操作失败", 0L, data);
    }
}
