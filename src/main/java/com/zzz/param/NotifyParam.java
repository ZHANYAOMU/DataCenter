package com.zzz.param;

import lombok.Data;

import java.io.Serializable;

@Data
public class NotifyParam implements Serializable {
    int id;
    String username;
    String notifyType;
    String[] toBeNotify;
}
