package com.zzz.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StringDifference {
    public static String[] getDifference(String str1, String str2) {

        Set<String> set1 = new HashSet<>(Arrays.asList(str1.split("\\|")));
        Set<String> set2 = new HashSet<>(Arrays.asList(str2.split("\\|")));

        set1.removeAll(set2);

        return set1.toArray(new String[0]);
    }
}
