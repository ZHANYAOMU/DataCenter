package com.zzz.service;

import com.zzz.dao.NotifyDao;
import com.zzz.domain.Notify;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NotifyService {
    private NotifyDao notifyDao;
    @Autowired
    public NotifyService(NotifyDao notifyDao) {
        this.notifyDao = notifyDao;
    }
    public void addNotify(Notify notify) {
        notifyDao.addNotify(notify);
    }
    public int updateNotifyUser(int id,String username) {
        return notifyDao.updateNotifyUser(id, username);
    }

}
