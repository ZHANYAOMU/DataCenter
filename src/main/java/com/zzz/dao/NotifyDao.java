package com.zzz.dao;

import com.zzz.domain.Notify;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface NotifyDao {
    int addNotify(Notify notify);
    List<Notify> selectNotify();
    int updateNotify(@Param("id") int id);
    int updateNotifyUser(@Param("id") int id, @Param("username") String username);
}
