package com.zzz.schedule;

import com.zzz.constant.AppConstant;
import com.zzz.dao.NotifyDao;
import com.zzz.domain.Notify;
import com.zzz.param.NotifyParam;
import com.zzz.util.StringDifference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class ScheduleTask {
    private NotifyDao notifyDao;
    @Autowired
    public ScheduleTask(NotifyDao notifyDao) {
        this.notifyDao = notifyDao;
    }
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Scheduled(cron = "0 0/1 * * * ?")
    public void selectNotify() {
        List<Notify> notifies = notifyDao.selectNotify();
        for (Notify notify : notifies) {
            String toBeNotify = notify.getToBeNotify();
            String alreadyNotify = notify.getAlreadyNotify();
            String[] difference = StringDifference.getDifference(toBeNotify, alreadyNotify);
            if (difference.length == 0) {
                notifyDao.updateNotify(notify.getId());
            }
            NotifyParam notifyParam = new NotifyParam();
            notifyParam.setNotifyType(notify.getNotifyType());
            notifyParam.setToBeNotify(difference);
            notifyParam.setUsername(notify.getUsername());
            notifyParam.setId(notify.getId());
            rabbitTemplate.convertAndSend(AppConstant.queueName, notifyParam);
        }

    }
}
